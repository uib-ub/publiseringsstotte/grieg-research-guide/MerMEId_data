# Xml repo med ressurser fra ariel.uib.no

Filer automatisk hentet ned med ant, se https://gitlab.com/ubbdev/MerMEId-backup

Med tanke på filhistorikk, begynte vi repositoriet med en kopi av filer hentet fra Oslo. Sigge kjørte igjennom 5-6 transformasjoner for å oppgradere de fra 2.x til 3.0 i versjon av MEI. Tenkte det var Ok å fortsatt ha en kopi av kildefilene i tilfelle noe ikke var optimalt i oppdateringen. 

Filer bør på et tidspunkt ryddes i. Ganske mange er ikke valid versjon 3.0 av MEI.
